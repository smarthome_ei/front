package fr.smartroom.front;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DashboardController {

    private GraphController graphController;
    private Requete requete;

    @FXML
    private HBox OverviewButton1;
    @FXML
    private HBox OverviewButton2;
    @FXML
    private Label OverviewName1;
    @FXML
    private Label OverviewName2;
    @FXML
    private Label OverviewResult1;
    @FXML
    private Label OverviewResult2;
    @FXML
    private HBox HBox1;
    @FXML
    private HBox HBox2;
    @FXML
    private HBox HBox3;
    @FXML
    private HBox HBox4;
    private Timeline timeline;

    private ArrayList<HBox> Row = new ArrayList<>();
    static ArrayList<Capteur> capteurList = new ArrayList<>();

    @FXML
    public void initialize() throws Exception {
        SetupSensor();
        timeline = new Timeline(new KeyFrame(Duration.seconds(5), e -> {
            try {
                UpdateSensor();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.playFromStart();
    }

    public void SetupSensor() throws Exception {
        Row.clear();
        Row.add(HBox1);
        Row.add(HBox2);
        Row.add(HBox3);
        Row.add(HBox4);


        if (capteurList.isEmpty()) {

            String data = requete.main("/sensors");

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                System.out.println(objectMapper.readValue(data, new TypeReference<List<SensorIni>>() {
                }));
                List<SensorIni> sensorsIni = objectMapper.readValue(data, new TypeReference<List<SensorIni>>() {
                });

                for (SensorIni sensor : sensorsIni) {
                    Capteur capteur = new Capteur();
                    String value = Requete.main("/sensors/" + sensor.getId());
                    ValueIni valueIni = objectMapper.readValue(value, new TypeReference<ValueIni>() {
                    });
                    if (!(valueIni.getLastValue() == null)) {
                        capteur.name = sensor.getType().getName();
                        capteur.lastValue = String.valueOf(valueIni.getLastValue().getValue());
                        capteur.lastDateTime = LocalDateTime.MIN;
                        capteur.status = !(valueIni.getValues() == null);
                        capteur.id = sensor.getId();
                        for (Value val : valueIni.getValues()) {
                            Mesure mesure = new Mesure();
                            mesure.dateTime = val.getDateTime();
                            mesure.value = val.getValue();
                            capteur.mesures.add(mesure);
                            String dateTimeStr = val.getDateTime();
                            if (dateTimeStr.length() < 20) {
                                dateTimeStr += ".";
                            }
                            if (dateTimeStr.length() <= 23) {
                                for (int i = dateTimeStr.length(); i < 23; i++) {
                                    dateTimeStr += "0";
                                }
                            }
                            mesure.value = val.getValue();
                            LocalDateTime dT = LocalDateTime.from(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(dateTimeStr));
                            if(capteur.lastDateTime.isBefore(dT))
                                capteur.lastDateTime = dT;
                        }

                        capteurList.add(capteur);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        OverviewResult1.setText(String.valueOf(Math.min(16, capteurList.size())));

        for (HBox r : Row) {
            r.getChildren().clear();
        }
        int num = Integer.parseInt(OverviewResult1.getText());
        int row = 0;
        for (int i = 0; i < num; i++) {
            Sensor sensor = new Sensor();
            sensor.num = i + 1;
            // Assuming CreateBox method returns a Label
            sensor.CreateBox(capteurList.get(i).name, capteurList.get(i).status, capteurList.get(i).lastValue, capteurList.get(i).name, capteurList.get(i));
            Row.get(row).getChildren().add(sensor.box);
            if ((i + 1) % 4 == 0) {
                row++;
            }
        }

        Date today = Calendar.getInstance().getTime();
        OverviewResult2.setText(String.valueOf(today));
    }





    public void UpdateSensor() throws Exception {


        try {
            ObjectMapper objectMapper = new ObjectMapper();


            for (Capteur capteur : capteurList) {
                String value = Requete.main("/sensors/" + capteur.id+"?start="+String.valueOf(capteur.lastDateTime));
                ValueIni valueIni = objectMapper.readValue(value, new TypeReference<ValueIni>() {
                });
                if (!(valueIni.getLastValue() == null)) {
                    capteur.lastValue = String.valueOf(valueIni.getLastValue().getValue());
                    capteur.status = !(valueIni.getValues() == null);
                    for (Value val : valueIni.getValues()) {
                        Mesure mesure = new Mesure();
                        mesure.dateTime = val.getDateTime();
                        mesure.value = val.getValue();
                        capteur.mesures.add(mesure);
                        String dateTimeStr = val.getDateTime();
                        if (dateTimeStr.length() < 20) {
                            dateTimeStr += ".";
                        }
                        if (dateTimeStr.length() <= 23) {
                            for (int i = dateTimeStr.length(); i < 23; i++) {
                                dateTimeStr += "0";
                            }
                        }
                        mesure.value = val.getValue();
                        LocalDateTime dT = LocalDateTime.from(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(dateTimeStr));
                        if(capteur.lastDateTime.isBefore(dT))
                            capteur.lastDateTime = dT;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Date today = Calendar.getInstance().getTime();
        OverviewResult2.setText(String.valueOf(today));

    }


    @FXML
    void OverviewButtonClick1(MouseEvent event) throws Exception {
        SetupSensor();

    }

    @FXML
    void OverviewButtonClick2(MouseEvent event) throws Exception {
        UpdateSensor();
    }

    @FXML
    void SensorButtonClick(MouseEvent event) throws IOException {
    }

    public void GotToGraph(Capteur capteur) throws Exception {
        HelloApplication m = new HelloApplication();
        graphController = new GraphController();
        graphController.Setup(capteur);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setController(graphController);
        fxmlLoader.setLocation(getClass().getResource("/Graph.fxml"));
        Parent root1 = (Parent)fxmlLoader.load();
        m.changeScene(root1);
    }

}


class SensorIni {
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private SensorType type;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    // Getter and setter for type
    public SensorType getType() {
        return type;
    }
    public void setType(SensorType type) {
        this.type = type;
    }
    // Getters and setters
}

class SensorType {
    @JsonProperty("name")
    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // Getters and setters
}

class ValueIni {
    @JsonProperty("id")
    private String id;
    @JsonProperty("lastValue")
    private LastValue lastValue;
    @JsonProperty("values")
    private List<Value> values;

    public LastValue getLastValue(){
        return lastValue;
    }

    public List<Value> getValues(){
        return values;
    }

    public String getId() {
        return id;
    }

}

class LastValue {
    private String dateTime;
    private float value;

    @JsonProperty("dateTime")
    public String getDateTime() {
        return dateTime;
    }

    @JsonProperty("value")
    public float getValue() {
        return value;
    }
}

class Value{
    private String dateTime;
    private float value;

    @JsonProperty("dateTime")
    public String getDateTime() {
        return dateTime;
    }

    @JsonProperty("value")
    public float getValue() {
        return value;
    }
}