package fr.smartroom.front;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class Requete {

        public static String main(String args) throws Exception {
            URL obj = new URL("http://ei-st5-405008.ew.r.appspot.com" + args);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            //con.setRequestProperty("User-Agent", USER_AGENT);
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);

                }
                in.close();


                return response.toString();


            } else {
                System.out.println("GET request did not work.");
                return "Error";
            }


        }
    }