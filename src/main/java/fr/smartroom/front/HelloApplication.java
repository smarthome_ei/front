package fr.smartroom.front;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class HelloApplication extends Application {
    private static Stage stg;

    private DashboardController dashboardController;


    @Override
    public void start(Stage stage) throws Exception {
        stg = stage;
        URL url = HelloApplication.class.getResource("/Dashboard.fxml");
        System.out.println(url);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(url);
        Scene scene = new Scene(fxmlLoader.load(), 1230, 680);
        stage.setTitle("Dashboard!");
        stage.setScene(scene);
        stage.show();

    }

    public void changeScene(Parent pane) {
        stg.getScene().setRoot(pane);
    }

    public static void main(String[] args) {
        launch();
    }
}