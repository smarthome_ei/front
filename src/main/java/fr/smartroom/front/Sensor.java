package fr.smartroom.front;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.io.IOException;
import java.net.URL;

public class Sensor {

    DashboardController dashboardController = new DashboardController();
    public int num;
    @FXML
    public HBox box = new HBox();
    public ImageView image = new ImageView();
    public VBox textBox = new VBox();
    public Label sensorName = new Label();
    private String stringSensorName;
    public Label status = new Label();
    private String stringStatus = "[Online]";
    public Label data = new Label();
    private String stringData;
    private String stringImage;

    private void Initiate(Capteur capteur){
        box.setId("SensorButton"+num);
        EventHandler<MouseEvent> s = e-> {
            try {
                dashboardController.GotToGraph(capteur);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        };
        box.setOnMouseClicked(s);
        box.setPrefSize(300,100);
        box.setStyle("-fx-border-radius: 25; -fx-border-style: solid;");
        box.setMargin(box, new Insets(0,0,0,5));

        URL url = getClass().getResource(stringImage);
        Image im = new Image(url.toString());
        box.getChildren().addAll(image, textBox);
        image.setId("SensorImage"+num);
        image.setFitHeight(100);
        image.setFitWidth(100);
        image.setPickOnBounds(true);
        image.setPreserveRatio(true);
        image.setImage(im);
        box.setMargin(image, new Insets(5,5,0,5));

        textBox.setPrefSize(193,30);

        sensorName.setId("SensorName"+num);
        sensorName.setPrefSize(192,30);
        sensorName.setFont(new Font("Calibri Bold",20));
        sensorName.setText(stringSensorName);

        status.setId("SensorStatus"+num);
        status.setPrefSize(190,26);
        status.setText(stringStatus);
        status.setFont(new Font(15));

        data.setId("SensorResult"+num);
        data.setPrefSize(190,60);
        data.setText(stringData);
        data.setFont(new Font(15));

        textBox.getChildren().addAll(sensorName,status,data);


    }


    public void CreateBox(String Name, Boolean Stat, String Da , String Im, Capteur capteur){
        stringSensorName = Name;
        if (!Stat){
            stringStatus = "[Offline]";
        }
        stringData = Da;
        stringImage = "/images/"+Im+".png";

        Initiate(capteur);
    }


}
