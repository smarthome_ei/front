package fr.smartroom.front;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.util.Duration;
import javafx.util.StringConverter;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class GraphController {

    private DashboardController dashboardController;
    @FXML
    private Button BackButton;

    @FXML
    private ToggleGroup ChooseTime;

    @FXML
    private DatePicker EDDate;

    @FXML
    private Spinner<Integer> EDHour;

    @FXML
    private Spinner<Integer> EDMinute;

    @FXML
    private LineChart<Number,Number> Graph;

    @FXML
    private RadioButton RealTime;

    @FXML
    private DatePicker SDDate;

    @FXML
    private Spinner<Integer> SDHour;

    @FXML
    private Spinner<Integer> SDMinute;

    @FXML
    private Button Send;

    @FXML
    private Label SensorId;

    @FXML
    private Label SensorName;

    @FXML
    private RadioButton SetTime;
    @FXML
    private Label ut;
    @FXML
    private NumberAxis xAxis;
    @FXML
    private NumberAxis yAxis;

    public Capteur capteur;
    private Timeline timeline;

    @FXML
    void Update() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String value = Requete.main("/sensors/" + capteur.id + "?start=" + String.valueOf(capteur.lastDateTime));
            System.out.println(value);
            System.out.println(capteur.lastDateTime);
            ValueIni valueIni = objectMapper.readValue(value, new TypeReference<ValueIni>() {
            });
            System.out.println(valueIni.getValues().isEmpty());
            if (!(valueIni.getValues().isEmpty())) {
                capteur.lastValue = String.valueOf(valueIni.getLastValue().getValue());
                capteur.status = !(valueIni.getValues() == null);
                for (Value val : valueIni.getValues()) {
                    Mesure mesure = new Mesure();
                    mesure.dateTime = val.getDateTime();
                    String dateTimeStr = val.getDateTime();
                    if (dateTimeStr.length() < 20) {
                        dateTimeStr += ".";
                    }
                    if (dateTimeStr.length() <= 23) {
                        for (int i = dateTimeStr.length(); i < 23; i++) {
                            dateTimeStr += "0";
                        }
                    }
                    mesure.value = val.getValue();
                    LocalDateTime dT = LocalDateTime.from(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(dateTimeStr));
                    if(capteur.lastDateTime.isBefore(dT))
                        capteur.lastDateTime = dT;
                    capteur.mesures.add(mesure);
                }


            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }


        Date today = Calendar.getInstance().getTime();
        ut.setText(String.valueOf(today));

        DrawGraph(capteur.mesures);

    }

    @FXML
    void BackButtonClick(ActionEvent event) throws Exception {
        timeline.stop();
        HelloApplication m = new HelloApplication();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Dashboard.fxml"));
        Parent root1 = (Parent)fxmlLoader.load();
        dashboardController = fxmlLoader.getController();
        m.changeScene(root1);
        dashboardController.SetupSensor();

    }

    void Setup(Capteur capteur) throws Exception {
        this.capteur = capteur;
    }

    void DrawGraph(List<Mesure> mesures) {
        Graph.getData().clear();
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        series.setName(capteur.name + " " + "Real Time");

        // Define the date-time formatter with milliseconds
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        long xmini = 10000000000000000l;
        long xmaxi = 0;

        float ymini = 100000000000f;
        float ymaxi = -1000000000000f;
        LocalDate sdate = SDDate.getValue();
        LocalDate edate = EDDate.getValue();
        int shour = SDHour.getValue();
        int ehour = EDHour.getValue();
        int sminute = SDMinute.getValue();
        int eminute = EDMinute.getValue();
        long sdatet = 0;
        if (sdate != null)
        {
            sdatet = sdate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() + 3600000l*shour + 60000l*sminute;
        }
        long edatet = 1000000000000000000l;
        if (edate != null)
        {
            edatet = edate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() + 3600000l*ehour + 60000l*eminute;
        }
        for (Mesure mesure : mesures) {
            String dateTimeStr = mesure.dateTime;
            if (dateTimeStr.length() < 20) {
                dateTimeStr += ".";
            }
            if (dateTimeStr.length() <= 23) {
                for (int i = dateTimeStr.length(); i < 23; i++) {
                    dateTimeStr += "0";
                }
            }
            LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, formatter);
            // Convert LocalDateTime to a numerical value (e.g., seconds since epoch)
            ZonedDateTime zdt = dateTime.atZone(ZoneId.systemDefault());
            long xValue = zdt.toInstant().toEpochMilli();
            if (xValue < edatet && xValue > sdatet)
            {
                xmini = Math.min(xmini, xValue);
                xmaxi = Math.max(xmaxi, xValue);
                ymini = Math.min(ymini, mesure.value);
                ymaxi = Math.max(ymaxi, mesure.value);

                series.getData().add(new XYChart.Data<>(xValue, mesure.value));
            }
        }

        // Add data points to the series
        Graph.getData().add(series);
        xAxis.setAutoRanging(false);
        yAxis.setAutoRanging(false);
        xAxis.setLowerBound(xmini);
        xAxis.setUpperBound(xmaxi);
        xAxis.setTickUnit((xmaxi-xmini)/5);
        yAxis.setUpperBound(1.1*ymaxi);
        yAxis.setLowerBound(0.9*ymini);

    }
    @FXML
    public void initialize()
    {
        SpinnerValueFactory<Integer> sheures = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,23,0);
        SpinnerValueFactory<Integer> sminutes = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,59,0);
        SpinnerValueFactory<Integer> heures = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,23,0);
        SpinnerValueFactory<Integer> minutes = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,59,0);
        SDHour.setValueFactory(heures);
        SDMinute.setValueFactory(minutes);
        EDHour.setValueFactory(sheures);
        EDMinute.setValueFactory(sminutes);
        DrawGraph(this.capteur.mesures);
        SensorId.setText(capteur.id);
        SensorName.setText(capteur.name);
        Date today = Calendar.getInstance().getTime();
        ut.setText(String.valueOf(today));
        xAxis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                Instant instant = Instant.ofEpochMilli(object.longValue());

                ZoneId zoneId = ZoneId.systemDefault(); // Use the system default time zone
                LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd HH:mm");
                return localDateTime.format(formatter);
            }

            @Override
            public Number fromString(String string) {
                return 0;
            }
        });
        timeline = new Timeline(new KeyFrame(Duration.seconds(5),e -> Update()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.playFromStart();
    }
}
