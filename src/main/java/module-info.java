module com.example.ei {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.fasterxml.jackson.databind;

    opens fr.smartroom.front to javafx.fxml, com.fasterxml.jackson.databind;
    exports fr.smartroom.front;

}

